package ru.t1.azarin.tm;

import ru.t1.azarin.tm.constant.ArgumentConst;

import ru.t1.azarin.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        if (processArguments(args)) {
            exit();
            return;
        }

        System.out.println("** WELCOME TO TASK-MANAGER **");
        final Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("ENTER THE COMMAND:");
            String command = scanner.nextLine();
            processCommands(command);
        }
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;

        final String param = args[0];

        switch (param) {
            case ArgumentConst.CMD_VERSION:
                showVersion();
                break;
            case ArgumentConst.CMD_ABOUT:
                showAbout();
                break;
            case ArgumentConst.CMD_HELP:
                showHelp();
                break;
            default:
                showErrorArg();
        }

        return true;
    }

    private static void processCommands(final String command) {
        if (command == null || command.isEmpty()) {
            showErrorCmd();
            return;
        }

        switch (command) {
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
            default:
                showErrorCmd();
        }
    }

    public static void showErrorCmd() {
        System.out.println("Wrong command. Try again with another command.");
    }

    public static void showErrorArg() {
        System.out.println("Wrong argument. Try again with another argument.");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Dmitry Azarin");
        System.out.println("azarindmitry1@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Display application version.\n", TerminalConst.CMD_VERSION, ArgumentConst.CMD_VERSION);
        System.out.printf("%s, %s - Display developer info.\n", TerminalConst.CMD_ABOUT, ArgumentConst.CMD_ABOUT);
        System.out.printf("%s, %s - Show application command.\n", TerminalConst.CMD_HELP, ArgumentConst.CMD_HELP);
    }

    public static void exit() {
        System.exit(0);
    }

}
