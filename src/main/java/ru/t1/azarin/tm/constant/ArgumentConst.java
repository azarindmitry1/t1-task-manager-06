package ru.t1.azarin.tm.constant;

public final class ArgumentConst {

    public static final String CMD_VERSION = "-v";

    public static final String CMD_ABOUT = "-a";

    public static final String CMD_HELP = "-h";

}
